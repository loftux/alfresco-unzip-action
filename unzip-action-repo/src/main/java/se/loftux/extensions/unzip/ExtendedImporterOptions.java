package se.loftux.extensions.unzip;

import org.alfresco.service.cmr.action.Action;

/**
 * Created by bhagyasilva on 23/04/15.
 */
public class ExtendedImporterOptions {

    public ExtendedImporterOptions(Action ruleAction) {
        String duplicateHandling_ = (String) ruleAction.getParameterValue(ExtendedImporterActionExecuter.PARAM_DUPLICATE_HANDLING_OPTION);
        if ("abort".equals(duplicateHandling_)) {
            duplicateHandlingOption = DuplicateHandlingOption.ABORT;
        } else if ("create-new-version".equals(duplicateHandling_)) {
            duplicateHandlingOption = DuplicateHandlingOption.CREATE_NEW_VERSION;
        } else if ("rename-existing-files".equals(duplicateHandling_)) {
            duplicateHandlingOption = DuplicateHandlingOption.RENAME_EXISTING_FILES;
        } else if ("rename-existing-files-and-folders".equals(duplicateHandling_)) {
            duplicateHandlingOption = DuplicateHandlingOption.RENAME_EXISTING_FILES_AND_FOLDERS;
        }else{
            //defualt to ABORT
            duplicateHandlingOption = DuplicateHandlingOption.ABORT;
        }
    }

    public DuplicateHandlingOption getDuplicateHandlingOption() {
        return duplicateHandlingOption;
    }

    public void setDuplicateHandlingOption(DuplicateHandlingOption duplicateHandlingOption) {
        this.duplicateHandlingOption = duplicateHandlingOption;
    }

    private DuplicateHandlingOption duplicateHandlingOption;
}
