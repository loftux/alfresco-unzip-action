package se.loftux.extensions.unzip;

/**
 * Created by bhagyasilva on 23/04/15.
 */
public enum DuplicateHandlingOption {
    ABORT, CREATE_NEW_VERSION, RENAME_EXISTING_FILES, RENAME_EXISTING_FILES_AND_FOLDERS
}
