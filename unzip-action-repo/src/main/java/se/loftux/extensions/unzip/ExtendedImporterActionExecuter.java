package se.loftux.extensions.unzip;

/**
 * Created by bhagyasilva on 23/04/15.
 */

import org.alfresco.error.AlfrescoRuntimeException;
import org.alfresco.model.ApplicationModel;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.action.ParameterDefinitionImpl;
import org.alfresco.repo.action.executer.ActionExecuterAbstractBase;
import org.alfresco.repo.content.MimetypeMap;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.repo.version.VersionModel;
import org.alfresco.service.cmr.action.Action;
import org.alfresco.service.cmr.action.ParameterDefinition;
import org.alfresco.service.cmr.dictionary.DataTypeDefinition;
import org.alfresco.service.cmr.model.FileExistsException;
import org.alfresco.service.cmr.model.FileFolderService;
import org.alfresco.service.cmr.model.FileInfo;
import org.alfresco.service.cmr.repository.*;
import org.alfresco.service.cmr.version.Version;
import org.alfresco.service.cmr.version.VersionService;
import org.alfresco.service.cmr.version.VersionType;
import org.alfresco.service.cmr.view.ImporterService;
import org.alfresco.service.namespace.QName;
import org.alfresco.service.transaction.TransactionService;
import org.alfresco.util.TempFileProvider;
import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipFile;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.transaction.*;
import java.io.*;
import java.text.MessageFormat;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipException;

/**
 * Importer action executor
 *
 * @author gavinc
 * @author bhagyas
 */
public class ExtendedImporterActionExecuter extends ActionExecuterAbstractBase {
    public static final String NAME = "extended-import";
    public static final String PARAM_ENCODING = "encoding";
    public static final String PARAM_DESTINATION_FOLDER = "destination";
    public static final String PARAM_DUPLICATE_HANDLING_OPTION = "duplicateHandling";

    private static final int BUFFER_SIZE = 16384;
    private static final String TEMP_FILE_PREFIX = "alf";
    private static final String TEMP_FILE_SUFFIX_ACP = ".acp";
    private static final String TEMP_FILE_SUFFIX_ZIP = ".zip";

    private boolean highByteZip = false;

    /**
     * The node service
     */
    private NodeService nodeService;

    public void setTransactionService(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    /**
     *
     */
    private TransactionService transactionService = null;

    /**
     * The content service
     */
    private ContentService contentService;

    /**
     * The file folder service
     */
    private FileFolderService fileFolderService;

    private static Log logger = LogFactory.getLog(ExtendedImporterActionExecuter.class);

    /**
     * Sets the NodeService to use
     *
     * @param nodeService The NodeService
     */
    public void setNodeService(NodeService nodeService) {
        this.nodeService = nodeService;
    }

    /**
     * Sets the ContentService to use
     *
     * @param contentService The ContentService
     */
    public void setContentService(ContentService contentService) {
        this.contentService = contentService;
    }

    /**
     * Sets the FileFolderService to use
     *
     * @param fileFolderService The FileFolderService
     */
    public void setFileFolderService(FileFolderService fileFolderService) {
        this.fileFolderService = fileFolderService;
    }

    /**
     * @return the highByteZip encoding switch
     */
    public boolean isHighByteZip() {
        return this.highByteZip;
    }

    /**
     * @param highByteZip the encoding switch for high-byte ZIP filenames to set
     */
    public void setHighByteZip(boolean highByteZip) {
        this.highByteZip = highByteZip;
    }

    /**
     * @see org.alfresco.repo.action.executer.ActionExecuter#executeImpl(org.alfresco.service.cmr.repository.NodeRef, org.alfresco.service.cmr.repository.NodeRef)
     */
    @Override
    public void executeImpl(Action ruleAction, NodeRef actionedUponNodeRef) {
        if (this.nodeService.exists(actionedUponNodeRef) == true) {
            // The node being passed in should be an Alfresco content package
            ContentReader reader = this.contentService.getReader(actionedUponNodeRef, ContentModel.PROP_CONTENT);
            if (reader != null) {
                NodeRef importDest = (NodeRef) ruleAction.getParameterValue(PARAM_DESTINATION_FOLDER);

                if (MimetypeMap.MIMETYPE_ZIP.equals(reader.getMimetype())) {
                    // perform an import of a standard ZIP file
                    ZipFile zipFile = null;
                    File tempFile = null;
                    try {
                        tempFile = TempFileProvider.createTempFile(TEMP_FILE_PREFIX, TEMP_FILE_SUFFIX_ZIP);
//                        tempFile = TempFileProvider.createTempFile(TEMP_FILE_PREFIX, TEMP_FILE_SUFFIX_ACP);
                        reader.getContent(tempFile);
                        // NOTE: This encoding allows us to workaround bug:
                        //       http://bugs.sun.com/bugdatabase/view_bug.do;:WuuT?bug_id=4820807
                        // We also try to use the extra encoding information if present
                        String encoding = (String) ruleAction.getParameterValue(PARAM_ENCODING);
                        if (encoding == null) {
                            encoding = "UTF-8";
                        } else {
                            if (encoding.equalsIgnoreCase("default")) {
                                encoding = null;
                            }
                        }
                        zipFile = new ZipFile(tempFile, encoding, true);
                        // build a temp dir name based on the ID of the noderef we are importing
                        // also use the long life temp folder as large ZIP files can take a while
                        File alfTempDir = TempFileProvider.getLongLifeTempDir("import");
                        File tempDir = new File(alfTempDir.getPath() + File.separatorChar + actionedUponNodeRef.getId());
                        try {
                            // TODO: improve this code to directly pipe the zip stream output into the repo objects -
                            //       to remove the need to expand to the filesystem first?
                            extractFile(zipFile, tempDir.getPath());

                            UserTransaction userTransaction=transactionService.getUserTransaction();

                            userTransaction.begin();
                            importDirectory(tempDir.getPath(), importDest, new ExtendedImporterOptions(ruleAction));
                            userTransaction.commit();
                        } catch (HeuristicRollbackException e) {
                            e.printStackTrace();
                        } catch (RollbackException e) {
                            e.printStackTrace();
                        } catch (SystemException e) {
                            e.printStackTrace();
                        } catch (HeuristicMixedException e) {
                            e.printStackTrace();
                        } catch (NotSupportedException e) {
                            e.printStackTrace();
                        } finally {
                            deleteDir(tempDir);
                        }
                    } catch (IOException ioErr) {
                        throw new AlfrescoRuntimeException("Failed to import ZIP file.", ioErr);
                    } finally {
                        // now the import is done, delete the temporary file
                        if (tempFile != null) {
                            tempFile.delete();
                        }
                        if (zipFile != null) {
                            try {
                                zipFile.close();
                            } catch (IOException e) {
                                throw new AlfrescoRuntimeException("Failed to close zip package.", e);
                            }
                        }
                    }
                }
            }
        }
    }

    public void setVersionService(VersionService versionService) {
        this.versionService = versionService;
    }

    VersionService versionService;

    /**
     * Recursively import a directory structure into the specified root node
     *
     * @param dir  The directory of files and folders to import
     * @param root The root node to import into
     */
    private void importDirectory(String dir, NodeRef root, ExtendedImporterOptions options) {
        File topdir = new File(dir);
        for (File file : topdir.listFiles()) {
            try {
                if (file.isFile()) {
                    String fileName = file.getName();

                    // create content node based on the file name
                    FileInfo fileInfo = null;


                    NodeRef fileNodeRef = nodeService.getChildByName(root, ContentModel.ASSOC_CONTAINS, fileName);
                    switch (options.getDuplicateHandlingOption()) {
                        case CREATE_NEW_VERSION:
                            logger.info("FileDH: Creating new version for file : [" + fileName  + "]");

                            if(fileNodeRef != null){
                                logger.info("FileDH: Existing file found. Creating new version...");
                                //Create new version after fetching the existing file.
                                Map<String, Serializable> versionProperties = new HashMap();
                                versionProperties.put(VersionModel.PROP_VERSION_TYPE, VersionType.MINOR);
                                Version version = versionService.createVersion(fileNodeRef, versionProperties);
                                fileInfo = this.fileFolderService.getFileInfo(fileNodeRef);
                                break;
                            }

                        case RENAME_EXISTING_FILES:
                        case RENAME_EXISTING_FILES_AND_FOLDERS:
                            logger.info(MessageFormat.format("FileDH: Renaming existing file ({0}) and create a new file.", fileName));

                            if(fileNodeRef != null){
                                logger.info("FileDH: Existing file found. Creating a new file...");
                                //Get a new name and create a new file with the new name and pass it to hte nodeService
                                NodeRef existingFileNodeRef = nodeService.getChildByName(root, ContentModel.ASSOC_CONTAINS, fileName);
                                String uniqueFileName = getUniqueFileName(fileName, existingFileNodeRef, root);
                                fileName =  uniqueFileName;
                                fileInfo = this.fileFolderService.create(root, uniqueFileName, ContentModel.TYPE_CONTENT);
                                logger.info(MessageFormat.format("FileDH: New file created with name: [{0}], NodeRef: [{1}]", fileName, fileInfo.getNodeRef()));
                                break;
                            }
                        case ABORT:
                            logger.info(MessageFormat.format("FileDH: Default option to abort if exists.", null));
                            fileInfo = this.fileFolderService.create(root, fileName, ContentModel.TYPE_CONTENT);
                    }

                    if(fileInfo == null){
                        logger.info("No file found, creating a file instead and returning a fileInfo object.");
                        fileInfo = this.fileFolderService.create(root, fileName, ContentModel.TYPE_CONTENT);
                    }


                    NodeRef fileRef = fileInfo.getNodeRef();

                    // add titled aspect for the read/edit properties screens
                    Map<QName, Serializable> titledProps = new HashMap<QName, Serializable>(1, 1.0f);
                    titledProps.put(ContentModel.PROP_TITLE, fileName);
                    this.nodeService.addAspect(fileRef, ContentModel.ASPECT_TITLED, titledProps);

                    // push the content of the file into the node
                    InputStream contentStream = new BufferedInputStream(new FileInputStream(file), BUFFER_SIZE);
                    ContentWriter writer = this.contentService.getWriter(fileRef, ContentModel.PROP_CONTENT, true);
                    writer.guessMimetype(fileName);
                    writer.putContent(contentStream);
                    logger.info(MessageFormat.format("File: Content write completed for node with nodeRef: [{0}]", fileRef));

                    //Create version 1 for any node which is not versioned.
                    if(!nodeService.hasAspect(fileRef, ContentModel.ASPECT_VERSIONABLE)){
                        Map<String, Serializable> versionProperties = new HashMap();
                        versionProperties.put(VersionModel.PROP_VERSION_TYPE, VersionType.MAJOR);
                        versionService.createVersion(fileRef, versionProperties);
                    }

                } else {
                    // create a folder based on the folder name
                    FileInfo folderInfo;

                    NodeRef folderNodeRef = nodeService.getChildByName(root, ContentModel.ASSOC_CONTAINS, file.getName());

                    switch (options.getDuplicateHandlingOption()) {
                        case RENAME_EXISTING_FILES_AND_FOLDERS:
                            if(folderNodeRef != null){
                                //Lookup folderNodeRef based on Name;
                                NodeRef existingFolderNodeRef = folderNodeRef;
                                String uniqueFileName = getUniqueFileName(file.getName(), existingFolderNodeRef, root);
                                folderInfo = this.fileFolderService.create(root, uniqueFileName, ContentModel.TYPE_FOLDER);
                                logger.info(MessageFormat.format("FolderDH: Create new named folder for existing is chosen as the option. Using existing folder with nodeRef: {0}", folderNodeRef));
                                break;
                            }
                        default:
                            //there is no versioning for folder nodes. hence falling back to the default
                            if(folderNodeRef == null){
                                folderInfo = this.fileFolderService.create(root, file.getName(), ContentModel.TYPE_FOLDER);
                            }else{
                                folderInfo = this.fileFolderService.getFileInfo(folderNodeRef);
                            }
                    }

                    NodeRef folderRef = folderInfo.getNodeRef();

                    // add the uifacets aspect for the read/edit properties screens
                    this.nodeService.addAspect(folderRef, ApplicationModel.ASPECT_UIFACETS, null);

                    importDirectory(file.getPath(), folderRef, options);
                    logger.info(MessageFormat.format("Folder: Importing folder ({0}) completed.", file.getName()));
                }
            } catch (FileNotFoundException e) {
                // TODO: add failed file info to status message?
                throw new AlfrescoRuntimeException("Failed to process ZIP file.", e);
            } catch (FileExistsException e) {
                // TODO: add failed file info to status message?
                throw new AlfrescoRuntimeException("File Exists in target.", e);
            }
            logger.info("Import successfully completed.");
        }
    }

    String getUniqueFileName(String filename, NodeRef existingFileNodeRef, NodeRef containerNode) {
        // Upload component was configured to find a new unique name for clashing filenames
        int counter = 1;
        String tmpFilename = filename;
        int dotIndex;

        while (existingFileNodeRef != null) {
            dotIndex = filename.lastIndexOf(".");
            if (dotIndex == 0) {
                // File didn't have a proper 'name' instead it had just a suffix and started with a ".", create "1.txt"
                tmpFilename = counter + filename;
            } else if (dotIndex > 0) {
                // Filename contained ".", create "filename-1.txt"
                tmpFilename = filename.substring(0, dotIndex) + "-" + counter + filename.substring(dotIndex);
            } else {
                // Filename didn't contain a dot at all, create "filename-1"
                tmpFilename = filename + "-" + counter;
            }

            existingFileNodeRef = nodeService.getChildByName(containerNode, ContentModel.ASSOC_CONTAINS, tmpFilename);
            counter++;
        }
        filename = tmpFilename;
        return filename;
    }

    /**
     * @see org.alfresco.repo.action.ParameterizedItemAbstractBase#addParameterDefinitions(java.util.List)
     */
    protected void addParameterDefinitions(List<ParameterDefinition> paramList) {
        paramList.add(new ParameterDefinitionImpl(PARAM_DESTINATION_FOLDER, DataTypeDefinition.NODE_REF,
                true, getParamDisplayLabel(PARAM_DESTINATION_FOLDER)));
        paramList.add(new ParameterDefinitionImpl(PARAM_ENCODING, DataTypeDefinition.TEXT,
                false, getParamDisplayLabel(PARAM_ENCODING)));
    }

    /**
     * Extract the file and folder structure of a ZIP file into the specified directory
     *
     * @param archive    The ZIP archive to extract
     * @param extractDir The directory to extract into
     */
    public static void extractFile(ZipFile archive, String extractDir) {
        String fileName;
        String destFileName;
        byte[] buffer = new byte[BUFFER_SIZE];
        extractDir = extractDir + File.separator;
        try {
            for (Enumeration e = archive.getEntries(); e.hasMoreElements(); ) {
                ZipArchiveEntry entry = (ZipArchiveEntry) e.nextElement();
                if (!entry.isDirectory()) {
                    fileName = entry.getName();
                    fileName = fileName.replace('/', File.separatorChar);
                    destFileName = extractDir + fileName;
                    File destFile = new File(destFileName);
                    String parent = destFile.getParent();
                    if (parent != null) {
                        File parentFile = new File(parent);
                        if (!parentFile.exists()) parentFile.mkdirs();
                    }
                    InputStream in = new BufferedInputStream(archive.getInputStream(entry), BUFFER_SIZE);
                    OutputStream out = new BufferedOutputStream(new FileOutputStream(destFileName), BUFFER_SIZE);
                    int count;
                    while ((count = in.read(buffer)) != -1) {
                        out.write(buffer, 0, count);
                    }
                    in.close();
                    out.close();
                } else {
                    File newdir = new File(extractDir + entry.getName());
                    newdir.mkdirs();
                }
            }
        } catch (ZipException e) {
            throw new AlfrescoRuntimeException("Failed to process ZIP file.", e);
        } catch (FileNotFoundException e) {
            throw new AlfrescoRuntimeException("Failed to process ZIP file.", e);
        } catch (IOException e) {
            throw new AlfrescoRuntimeException("Failed to process ZIP file.", e);
        }
    }

    /**
     * Recursively delete a dir of files and directories
     *
     * @param dir directory to delete
     */
    public static void deleteDir(File dir) {
        if (dir != null) {
            File elenco = new File(dir.getPath());

            // listFiles can return null if the path is invalid i.e. already been deleted,
            // therefore check for null before using in loop
            File[] files = elenco.listFiles();
            if (files != null) {
                for (File file : files) {
                    if (file.isFile()) file.delete();
                    else deleteDir(file);
                }
            }

            // delete provided directory
            dir.delete();
        }
    }
}
