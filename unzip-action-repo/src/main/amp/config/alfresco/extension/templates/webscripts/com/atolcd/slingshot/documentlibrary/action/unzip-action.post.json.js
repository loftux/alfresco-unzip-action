/**
 * Unzip method
 *
 * @method POST
 * @param nodeRef {String}
 */

function main()
{
   //try
   //{
    var archiveNode = null;
	if (json.has('archive-node')) {
	  archiveNode = search.findNode(json.get('archive-node'));
	}

	var encoding = "UTF8";
	if(json.has('encoding')) {
		encoding = json.get('encoding');
	}

	var destinationSpace = null;
    if (json.get('unzip') == 'other') {
		// unzip into a selected space
		if (json.has('destination-node')) {
		  destinationSpace = search.findNode(json.get('destination-node'));
		}
	} else {
		if (json.get('unzip') == 'new' && json.has('space-name')) {
			// unzip into a new space //TODO Add support for creating a new file
			var space = json.get('space-name');

			var existingSpace = archiveNode.parent.childByNamePath(space);

			if(existingSpace != null){
				destinationSpace = existingSpace;
			}else{
				destinationSpace = archiveNode.parent.createFolder(space);
			}

		} else {
			// unzip into current space
			destinationSpace = archiveNode.parent;
		}
	}

	var duplicateHandling = 'abort';
	if(json.has('duplicateHandling')){
		duplicateHandling = json.get('duplicateHandling')
	}

	// create unzip action
	var importer = actions.create("extended-import");
	importer.parameters.encoding = encoding;
	importer.parameters.destination = destinationSpace;
	importer.parameters.duplicateHandling = duplicateHandling; // {abort|create-new-version|rename-existing}


	status.code = 200;

	try {
      importer.execute(archiveNode);
	}
   catch (e)
   {
     status.code = 500;
     status.message = "Unexpected error occured during content extraction.";

     if (e.message && e.message.indexOf("File Exists") > 0)
     {
        status.code = 409;
        status.message = e.message;
     }
     return;
   }
}

main();